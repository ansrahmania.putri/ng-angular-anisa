import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { ResponseBase } from "../interfaces/response.interface";
import { UserList } from "../interfaces/user.interface";

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    private baseApi = 'http://localhost:8080';

    constructor(private httpClient: HttpClient) {}

    listUsers(): Observable<UserList[]> {

        // const token = localStorage.getItem('token')
        
        // const headers = new HttpHeaders({
        //     'Authorization': `Bearer ${token}`
        // })

        return this.httpClient.get<ResponseBase<UserList[]>>(this.baseApi + '/api/user/').pipe(
            map((val) => {
                return val.data;
            }),
            catchError((err) => {
                console.log(err);
                throw err
            })
        )
    }

    // uploadPhoto (data: File) Observeable<ResponseBase<ResponseUploadPhoto>> {
    //     const file = new FormData()
    //     file.append('file', data, data.name)
    //     return this.httpClient.post<ResponseBase<ResponseUploadPhoto>>(`${this.baseApi}/users/uploadPhoto`, file)
    // }

    listHistory(): Observable<History[]> {

        return this.httpClient.get<ResponseBase<History[]>>(this.baseApi + '/api/history/').pipe(
            map((val) => {
                return val.data;
            }),
            catchError((err) => {
                console.log(err);
                throw err
            })
        )
    }

    

    

    
}