import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  users!: User[];
  history!: History[];

  public baseHarga = 20000

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getUser(): Observable<User[]> {
    return this.httpClient.get<User[]> (this.baseApi + '/users')
  }

  // getHistory(): Observable<History[]> {
  //   return this.httpClient.get<History[]> (this.baseApi + '/api/history/')
  // }
}
  

