import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() title = 'ng-angular-anisa'

  @Input() name = 'Anisa'
  @Input() age = 30
  @Input() status = false

  @Output() dataCallBack = new EventEmitter()

  doClick() {
    this.dataCallBack.emit({ data: {name: this.name, age: this.age, status:this.status}})
  }

}
