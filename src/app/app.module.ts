import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { LoginComponent } from './pages/login/login.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { RegisterComponent } from './pages/register/register.component';
import { CalculateGasolineComponent } from './pages/calculate-gasoline/calculate-gasoline.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserComponent } from './pages/users/users.component';
import { AuthInterceptor } from './core/auth.interceptor';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SupervisorComponent } from './pages/supervisor/supervisor.component';
import { DashboardComponent } from './pages/supervisor/dashboard/dashboard.component';
import { LayoutsModule } from "./layouts/layouts.module";
@NgModule({
    declarations: [
        AppComponent,
        CardComponent,
        LoginComponent,
        PlaygroundComponent,
        RegisterComponent,
        CalculateGasolineComponent,
        UserComponent,
        // DashboardComponent,
        // UsersComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        ModalModule.forRoot(),
        LayoutsModule
    ]
})
export class AppModule {}
