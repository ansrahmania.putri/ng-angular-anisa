import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  username= ''
  email= ''
  password= ''

  formRegister: FormGroup

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {
    this.formRegister = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
    email: ['', [Validators.required]]
    })
    
  }

  get errorControl() {
    return this.formRegister.controls;
  }

  get confirmPassword(){
    return this.formRegister.value.password == this.formRegister.value.c_password 
  }

  doRegister() {
    const payload = {
      username: this.username,
      email: this.email,
      password: this.password
    }

    this.authService.register(payload).subscribe(
      response => {
        console.log(response)
      }
    )
  }


}
