import { Component, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { History } from 'src/app/interfaces/history.interface';
import { DataService } from 'src/app/services/data.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})

export class HistoryComponent {
  modalRef!: BsModalRef

  historyList = {
    idHistory: 0,
    totalParticipants: 0,
    totalItem: 0,
    dateSplitBill: '',
    timeSplitBill: '',
    totalAmount: 0,
  }

  history: any
  histordet: any
  constructor(private usersService: UsersService, private modalService: BsModalService) { 
    this.usersService.listHistory().subscribe(
      response => {
        this.history = response
        console.log(response)
      }  
    )
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template)
  }

  modalDetail(data: History) {
    console.log(data)
    this.historyList = data
    this.histordet = data.historyDet
    console.log(this.histordet)
  }

}





