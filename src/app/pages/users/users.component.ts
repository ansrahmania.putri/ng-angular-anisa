import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UserComponent {
  users: any
  constructor(private dataService: DataService) { 
    this.dataService.getUser().subscribe(
      response => {
        this.users = response
        console.log(response)
      }  
    )
  }
}

// export class UsersComponent {

//   constructor(private dataService: DataService) {
//     this.dataService.getUser().subscribe(
//       response => {
//         console.log(response)
//       }
//     )
//   }
// }
