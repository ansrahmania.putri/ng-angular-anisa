// import { Component, TemplateRef } from '@angular/core';
// import { switchMap } from 'rxjs';
// import { UsersService } from 'src/app/services/users.service';

// @Component({
//   selector: 'app-users',
//   templateUrl: './users.component.html',
//   styleUrls: ['./users.component.scss']
// })
// export class UsersComponent {
//   users: UserList[] = [];
//   modalRef?: BsModalRef;

//   photo!: string;
//   photoFile!: File;

//   constructor(
//     private userService: UsersService,
//     private modalService: BsModalService
//   ) {
//     this.userService.listUsers().subscribe((response) => {
//       console.log(response);
//       this.users = response;
//     });
//   }

//   openModal(template: TemplateRef<any>) {
//     this.modalRef = this.modalService.show(template);
//   }

//   showPreview(event: any) {
//     if (event) {
//       const file = event.target.files[0];
//       this.photoFile = file;
//       const reader = new FileReader();
//       reader.onload = () => {
//         this.photo = reader.result as string;
//       };
//       reader.readAsDataURL(file);
//     }
//   }

//   doAddUser() {
//     this.userService.uploadPhoto(this.photoFile).pipe(
//       switchMap(val => {
//         const payload
//         return this.authService.register(

//         )
//       })
//     )
//   }

// }

import { Component } from '@angular/core';
import { switchMap } from 'rxjs';
import { User, UserList } from 'src/app/interfaces/user.interface';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
// import { User } from '../interfaces/user.interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent {
  users!: User[]
  // modalRef!: BsModalRef
  // photo!: string
  // photoFile!: File

  // users: any
  constructor(private dataService: DataService) {
    this.dataService.getUser().subscribe(
      response => {
        console.log(response)
        this.users = response
      }
    )
  }

  // openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template)
  // }

  // showPreview(event: any) {
  //   if (event) {
  //     constfile = event.target.files[0]
  //     this.photoFile = File
  //     const reader = new FileReader()
  //     reader.onload = () => {
  //       this.photo = reader.result as string:
  //     }
  //     reader.readAsDataURL(file)
  //   }
  // }

  // doAddUser() {
  //   this.userServices.uploadPhoto(this.photoFile).pipe(
  //     switchMap(val => {


  //       return this AuthService.register(

  //       )
  //     })
  //   )
  // }

}