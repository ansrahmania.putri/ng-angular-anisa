import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { UsersComponent } from './product/users/users.component';
import { HistoryComponent } from '../history/history.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      },
      // {
      //   path: 'edit-profile',
      //   component: EditProfileComponent
      // },
    ]
  }
]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, SettingsComponent, UsersComponent, HistoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule
  ]
})
export class AdminModule { }