import { Component } from '@angular/core';
import { User, UserList } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {
  user: UserList[] = []
  constructor(private userService: UsersService ) {
    this.userService.listUsers().subscribe(
      response => {
        console.log(response)
        this.user = response
      }
    )
  }

  // history: History[] = []
  // constructor(private history: UsersService ) {
  //   this.userService.listHistory().subscribe(
  //     response => {
  //       console.log(response)
  //       this.history = response
  //     }
  //   )
  // }
}
