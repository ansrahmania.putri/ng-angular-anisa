import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupervisorComponent } from './supervisor.component';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LayoutsModule } from 'src/app/layouts/layouts.module';

const routes: Routes = [
  {
    path: '',
    component: SupervisorComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
    ]
  }
]

@NgModule({
  declarations: [SupervisorComponent, DashboardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule
  ]
})
export class SupervisorModule { }