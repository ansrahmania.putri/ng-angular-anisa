import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'ng-angulard1-anisa';

  name = 'Anisa'
  age = 20
  status = false

  person = {
    title: 'Test A',
    name: 'nganisa',
    age: 0,
    status: true
  };

  showData = true;

  nomor = 6;

  personList = [
    {
      title: 'Test A',
      name: 'nganisa',
      age: 0,
      status: true,
    },
    {
      title: 'Test B',
      name: 'ngrahmania',
      age: 20,
      status: false,
    }
  ];

  datas = [1, 2, 3]

  constructor() {
    this.name = 'Rahmania'
    this.age = 25
  }

  onCallBack(ev: any) {
    console.log(ev);
    this.personList.push(ev.data)
  }

}
