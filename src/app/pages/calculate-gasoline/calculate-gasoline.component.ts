import { Component, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {
  bensin1 = 'Pertalite'
  bensin2 = 'Pertamax'
  bensin3 = 'Bio-Solar'
  bensin4 = 'Pertamax Turbo'

  testClass = 'alert alert-success'

  public jumlahLiter = 0;
  public totalBayar = 0;
  public jumlahUang = 0;

  hargaPertalite = 10000;
  hargaPertamax = 14000;
  hargaBioSolar = 7000;
  hargaPertamaxTurbo = 15000;

  constructor(private dataService: DataService) {
    this.hargaPertalite = this.dataService.baseHarga
  }

  pertalite() {
    this.totalBayar = this.jumlahLiter * this.hargaPertalite
  }

  pertamax() {
    this.totalBayar = this.jumlahLiter * this.hargaPertamax
  }

  bioSolar() {
    this.totalBayar = this.jumlahLiter * this.hargaBioSolar
  }

  pertamaxTurbo() {
    this.totalBayar = this.jumlahLiter * this.hargaPertamaxTurbo
  }

  // @Output() dataCallBack = new EventEmitter()
  
  // doClickPertalite() {
  //   this.dataCallBack.emit({ data: {bensin1: this.bensin1, hargaPertalite:this.hargaPertalite}})
  // }

}
