import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  //kalo di form builder variabel kosong ini sudah tidak digunakan lagi
  username = '';
  password = '';

  formLogin: FormGroup
 
  //menunjukkan bahwa value username harus ada gaboleh kosong
  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required]]
    })
  }

  get errorControl() {
    return this.formLogin.controls;
  }

  doLogin() {

    console.log(this.formLogin)

    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password
    }

    this.authService.login(payload).subscribe(
      response => {
        console.log(response)
        if(response.data.token) {
          if(response.data.roles == "ADMIN") {
            localStorage.setItem('token', response.data.token)
            Swal.fire(
              'Success!',
              'You are logged in!',
              'success'
            )
            // alert("You have successfully log in!")
            console.log(response)
            this.router.navigate(['/admin/dashboard'])
          } else {
            alert("Only admin can access this page")
            console.log("Error, please try again")
            this.router.navigate(['/users'])
          }
        }
      }, error => {
        console.log(error)
        Swal.fire({
          title: 'Error!',
          text: 'You entered wrong username or password',
          icon: 'error',
          confirmButtonText: 'Try again'
        })
        // alert(error.error.message)
      }
      
    )
  }

}
