import { Time } from "@angular/common"

export interface History {
    idHistory: number,
    totalParticipants: number,
    totalItem: number,
    dateSplitBill: string,
    timeSplitBill: string,
    totalAmount: number,
    historyDet: [
      {
      idHistoryDet: number,
      idContact: number,
      amount: number,
      paymentStatus: string,
      paymentDate: string
      }
    ]
  }