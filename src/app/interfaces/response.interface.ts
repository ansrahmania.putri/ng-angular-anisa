export interface ResponseBase<T> {
    message: string;
    status: boolean;
    data: T;
}