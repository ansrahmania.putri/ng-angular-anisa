export interface User {
    id: number,
    name: string,
    phone: string,
    username: string,
    website: string,
    email: string,
    company: {
      bs: string,
      catchParse: string,
      name: string,
    },
    address: {
      city: string,
      street: string,
      suite: string,
      zipcode: string,
      geo: {
        lat: string,
        lng: string,
      }
    }
  }

  export interface UserList {
    idUser: number,
    username: string,
    email: string,
    roles: string,
    userDet: {
        fullName: string,
        birthDate: Date,
      }
  }