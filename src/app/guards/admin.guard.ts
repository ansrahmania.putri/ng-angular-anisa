import { Injectable } from '@angular/core';
import { CanLoad, Router, UrlSegment, UrlTree } from '@angular/router';
import { filter, map, Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})



export class AdminGuard implements CanLoad {
  constructor(private authService: AuthService, private router: Router) {}

  canLoad(): Observable<boolean> {
    return this.authService.isAdmin.pipe(
      filter((val) => val !== null),
      map((val) => {
        if (val) {
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      })
    );
  }  
}