import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlSegment, UrlTree } from '@angular/router';
import { filter, map, Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.authService.isUser.pipe(
      filter((val) => val !== null),
      map((val) => {
        if (val) {
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      })
    );
  }  
  
}
